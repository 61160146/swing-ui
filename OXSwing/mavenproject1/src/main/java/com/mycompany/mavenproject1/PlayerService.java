/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class PlayerService {
    static Player o,x;
    
    public static void load(){
        System.out.println("Load");
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            File file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (oos != null) {
                    oos.close();
                }
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static void save(){
       System.out.println("Save"); 
       FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            File file = new File("ox.bin");
            fos = new FileOutputStream(file);

            oos = new ObjectOutputStream(fos);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (oos != null) {
                    oos.close();
                }
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static Player getO() {
        return o;
    }

    public static Player getX() {
        return x;
    }
    
}
